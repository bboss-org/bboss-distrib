# BBOSS Copyright (C) 2018-2021, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# Define default values
$hyper_venv = {
  "VENV_SCRIPT_PROJ" => nil,
  "VENV_HYPER_BOX_NUM" => 1,
  "VENV_HYPER_BOX_IMG" => "ubuntu/bionic64",
  "VENV_HYPER_BOX_VER" => ">= 0",
  "VENV_HYPER_BOX_CPU" => 4,
  "VENV_HYPER_BOX_MEM" => 16384,
  "VENV_HYPER_BOX_SSH" => "127.0.0.1",
  "VENV_HYPER_NET_FQDN" => nil,
  "VENV_HYPER_NET_PORT" => "8443",  # HTTPS only
  "VENV_PROXY_DOCKER" => nil,
}

# Read default values from last run
hyper_config = File.join(File.dirname(__FILE__), ".vagrant/hyper_config.rb")
if File.exist?(hyper_config)
  require hyper_config
end

# Non empty environment variable overrides default saved values
File.open(hyper_config, "w") do |line|
  $hyper_venv.each_key do |key|
    value = "#{ENV[key]}"
    if value.empty?
      value = $hyper_venv[key]
    else
      $hyper_venv[key] = value
    end
    # Save default values for the next run
    line.puts "$hyper_venv[\"#{key}\"] = \"#{value}\""
  end
end

# Define supported Vagrant box characteristics
hyper_boxes = {
  "centos/7" =>         {netif: "eth1"},    # CentOS 7
  "debian/stretch64" => {netif: "eth1"},    # Debian 9
  "debian/buster64"  => {netif: "eth1"},    # Debian 10
  "ubuntu/bionic64"  => {netif: "enp0s8"},  # Ubuntu 18.04 LTS
  "ubuntu/focal64"   => {netif: "enp0s8"},  # Ubuntu 20.04.1 LTS
}
hyper_boxes.default = hyper_boxes["ubuntu/bionic64"]
hyper_box = hyper_boxes[$hyper_venv["VENV_HYPER_BOX_IMG"]]

# Link content of ./inventory into .vagrant/provisioners/ansible/inventory to
# reuse group definitions and group_vars.
hyper_inventory = File.join(File.dirname(__FILE__),
  ".vagrant", "provisioners", "ansible", "inventory")
FileUtils.mkdir_p(hyper_inventory) if ! File.exist?(hyper_inventory)
Dir.foreach("inventory") do |item|
  ln_lnk = File.join(hyper_inventory, item)
  ln_trg = File.join("..", "..", "..", "..", "inventory", item)
  FileUtils.ln_s(ln_trg, ln_lnk) if ! File.exist?(ln_lnk)
end

Vagrant.configure("2") do |config|
  config.ssh.insert_key = false
  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.cpus = $hyper_venv["VENV_HYPER_BOX_CPU"]
    vb.memory = $hyper_venv["VENV_HYPER_BOX_MEM"]
  end

  # Declare Vagrant host variables
  host_last = $hyper_venv["VENV_HYPER_BOX_NUM"].to_i - 1
  host_vars = {}
  host_grps = {
    "kube_master" => [],
    "kube_node" => [],
  }
  (0..host_last).each do |i|
    # Define Vagrant host characteristics
    host_name = "kube-#{i}"
    host_ip   = "172.16.50.#{10 + i}"
    host_vars[host_name] = {"kube_network_if" => hyper_box[:netif]}
    if i == 0
      host_grps["kube_master"] << host_name
    else
      host_grps["kube_node"] << host_name
    end

    config.vm.define host_name do |node|
      node.vm.box = $hyper_venv["VENV_HYPER_BOX_IMG"]
      node.vm.box_version = $hyper_venv["VENV_HYPER_BOX_VER"]
      node.vm.network "private_network", ip: host_ip
      # Override ssh forwarding to allow external access
      node.vm.network "forwarded_port", guest: 22, host: 2200 + i, id: "ssh",
        host_ip: $hyper_venv["VENV_SCT_HYPERSSH"], auto_correct: true

      # Forward master ports for external access
      if i == 0
        node.vm.network "forwarded_port", guest: 6443, host: 6443   # Cluster API HTTPS
        node.vm.network "forwarded_port", guest: 80, host: 8080     # Cluster HTTP
        node.vm.network "forwarded_port", guest: 443, host: 8443    # Cluster HTTPS
      end
      # Only execute once the Ansible provisioner, when all the machines are
      # up and ready.
      # https://www.vagrantup.com/docs/provisioning/ansible#ansible-parallel-execution
      if i == host_last
        node.vm.provision "ansible" do |ansible|
          ansible.compatibility_mode = "2.0"
          # Disable default limit to connect to all the machines
          ansible.limit = "all,localhost"
          ansible.groups = host_grps
          ansible.host_vars = host_vars
          ansible.playbook = "playbooks/install.yml"
        end
      end
    end
  end
end
